package types

type UserLoginReq struct {
	Username string `json:"username" form:"username"`
	Password string `json:"password" form:"password"`
}

type UserTokenData struct {
	User any `json:"user"`
	Token string `json:"token"`
}
