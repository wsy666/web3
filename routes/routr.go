package router

import (
	"github.com/gin-gonic/gin"
	"web3/api"
)

func NewRouter() *gin.Engine{
	r := gin.Default()
	r.GET("/", api.HandleCommon())

	// 登陆
	r.POST("/login", api.HandleLogin())


	return r
}