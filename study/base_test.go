package study

import (
	"fmt"
	"testing"
)

type person struct {
	Name string
	Age int
}

func TestSprintf(t *testing.T) {
	tsr := fmt.Sprintf("%s", "hello" )
	fmt.Println(tsr)
	tsr = fmt.Sprintf("%d", 100)
	fmt.Println(tsr)

	fmt.Println(fmt.Sprintf("%v", 100), "> %v")
	fmt.Println(fmt.Sprintf("%v", "hello"), "> %v")

	per := person{
		Name: "wsy",
		Age:  36,
	}
	fmt.Println(fmt.Sprintf("%v", per), "> %v")  // 简单输出结构体
	fmt.Println(fmt.Sprintf("%+v", per), "> %+v") // 打印出结构体的字段名和值
	fmt.Println(fmt.Sprintf("%#v", per), "> %#v") // 更详细,打印出包名和结构体体的字段名
	fmt.Println(fmt.Sprintf("%T", per), "> %T") // 只打印类型

	// %b 二进制
	fmt.Println(fmt.Sprintf("%b", 2), "> %b") // 10
	fmt.Println(fmt.Sprintf("%x", 210), "> %x") // 16进制 小写
	fmt.Println(fmt.Sprintf("%X", 210), "> %X") // 16进制 大写
	//fmt.Println(fmt.Sprintf("%U", "\u9090"), "> %U") // Unicode
	fmt.Println(fmt.Sprintf("%f", 210.12), "> %f") // 6 wei
	fmt.Println(fmt.Sprintf("%.3f", 210.12), "> %f") // 3 wei

	tsr = "hello"
	fmt.Println(fmt.Sprintf("%p", &tsr), "> %p") // 指针16进制的形式



}

