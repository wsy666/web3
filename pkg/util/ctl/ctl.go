package ctl

import "web3/pkg/e"

type Response struct {
	Status int `json:"status"`
	Msg string `json:"msg"`
	Data any `json:"data"`
}

func NewResponse(status int, data any) *Response  {
	return &Response{
		Status: status,
		Msg: e.GetMsg(status),
		Data: data,
	}
}

func DefaultResponse(data any) *Response {
	return NewResponse(e.SUCCESS, data)
}