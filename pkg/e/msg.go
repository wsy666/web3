package e

var MessageMap = map[int]string{
	SUCCESS: "执行成功",
	ERROR: "失败",
}

func GetMsg(status int) string {
	msg, ok :=  MessageMap[status]
	if ok{
		return msg
	}
	return MessageMap[ERROR]
}