package model_test

import (
	"fmt"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
	"testing"
	"time"
)

type MysqlConfig struct {
	Name     string
	Password string
	Host     string
	Port     int
	DbName   string
}

var sqlConfig = MysqlConfig{
	Name:     "root",
	Password: "wsy",
	Host:     "127.0.0.1",
	Port:     3306,
	DbName:   "gotest",
}

/*
DSN (Data Source Name)
*/

func NewMySqlDB() *gorm.DB {
	//dsn := "user:pass@tcp(127.0.0.1:3306)/dbname?charset=utf8mb4&parseTime=True&loc=Local"
	format := "%s:%s@tcp(%s:%d)/%s?charset=utf8mb4&parseTime=True&loc=Local"
	dsn := fmt.Sprintf(format,
		sqlConfig.Name,
		sqlConfig.Password,
		sqlConfig.Host,
		sqlConfig.Port,
		sqlConfig.DbName,
	)
	fmt.Println("dsn:", dsn)
	db, err := gorm.Open(mysql.Open(dsn), &gorm.Config{})
	if err != nil {
		return nil
	}
	return db
}
var db = NewMySqlDB()


/* ==================================================================================
create table
   ==================================================================================
*/
type Product struct {
	Code string
	Price uint
	gorm.Model
}

func TestCreateTable(t *testing.T)  {

	err := db.AutoMigrate(&Product{})
	if err != nil {
		fmt.Println(err.Error())
	}

	db.Create(&Product{Code: "D54", Price: 100})

	var product Product
	db.First(&product, 1)
	fmt.Println("first product: ", product)

	db.First(&product, "code = ?", "D54")
	fmt.Println("first product: ", product)

	db.Model(&product).Update("Price", 200)
	fmt.Println("first product: ", product)

	// Updates Product
	db.Model(&product).Updates(Product{Price: 300, Code: "D100"})
	fmt.Println("first product: ", product)
	// Updates  map
	db.Model(&product).Updates(map[string]any{"Price":400, "Code":"D99"})
	fmt.Println("first product: ", product)

	// delete
	db.Delete(&product, 1)

}

/* ==================================================================================
add data
   ==================================================================================
*/
func TestCreateRecord(t *testing.T) {
	pro1 := Product{
		Code:  "S11",
		Price: 11,
	}
	result := db.Create(&pro1) // result is DB type
	//fmt.Println("result: ", result)
	_ = result
}

func TestCreateRecords(t *testing.T) {
	pros := []Product{
		{Code: "S12", Price: 12},
		{Code: "S13", Price: 13},
		{Code: "S14", Price: 14},
	}
	result := db.Create(pros)
	fmt.Println("err: ", result.Error)
}

func TestCreateSelect(t *testing.T)  {
	pro := Product{
		Code:  "C11",
		Price: 111,
	}
	db.Select("Code").Create(&pro) // Price 不会写进去, 为空, 只把pro对象的Code字段的值写到数据库了
}

/* ==================================================================================
Hoot
   ==================================================================================
*/
func (p *Product) BeforeCreate(tx *gorm.DB) error {
	fmt.Println("BeforeCreate, Price is ", p.Price)
	return nil
}

func TestCreateWithHoot(t *testing.T) {
	pro := Product{Code: "C100", Price: 1000}
	db.Create(&pro)
	// 自动打印
}

/* ==================================================================================
Create from Map
   ==================================================================================
*/

func TestCreateFromMap(t *testing.T) {
	// one record
	// 注意里插入记录里面实际上没有创建日期等字段的内容
	db.Model(&Product{}).Create(map[string]any{
		"Code":"C2000", "Price":2000,
	})
}

/* ==================================================================================
Query
   ==================================================================================
*/

func TestQueryOne(t *testing.T) {
	var pro	Product
	db.First(&pro)
	fmt.Printf("find pro %v\n", pro)

	// Take
	var proTake Product
	result := db.Take(&proTake)
	fmt.Printf("find pro %v\n", proTake)
	fmt.Printf("Row Effected: %v\n", result.RowsAffected)

	// Map
	resultNew := map[string]any{}
	db.Model(&Product{}).First(&resultNew)
	fmt.Printf("map find product: %v\n", resultNew)


	// With Take
	resultTake := map[string]any{}
	db.Table("products").Take(&resultTake)
	fmt.Printf("takeProduct: %v\n", resultTake)

}

// 使用主键来检索对象
func TestQueryObjectWithPrimaryKey(t *testing.T) {
	var pro Product
	db.First(&pro, 4) // id is 4
	fmt.Printf("key 4: %v\n", pro)


	var pro1 Product
	db.First(&pro1, "4")
	fmt.Printf("key 4>string: %v\n", pro1)
	
	
	var pro2 = Product{Price: 400}
	db.First(&pro2)
	fmt.Printf("Price is 400 || %v\n", pro2)

	var pro3 Product
	db.First(&pro3, "Code = ?", "C11")
	fmt.Printf("Code is C11 || %v\n", pro3)

	var proModel Product
	//result := db.Model(Product{Price: 1000}).First(&proModel) // ????? Price is 400
	result := db.Model(Product{Code: "C100"}).First(&proModel) // ????? Price is 400
	fmt.Printf("Price=1000 || %v\n", proModel)
	fmt.Printf("Effect row is %d", result.RowsAffected)


}

func TestQueryObjectMany(t *testing.T) {
	var pros []Product
	db.Find(&pros, []int{1,2,3,4})
	if pros != nil {
		for i := 0; i < len(pros); i++ {
			fmt.Printf("i: %d, pro: %v\n", i, pros[i])
		}
	}
}


/*
String Conditions
*/
func PrintProdecut(pros []Product)  {
	for i := 0; i < len(pros); i++ {
		fmt.Printf("i: %d, pro: %v\n", i, pros[i])
	}
}

func TestQueryWithWhere(t *testing.T) {
	var pro	Product
	db.Where("Code = ?", "C100").First(&pro)
	fmt.Printf("Code=100 || %v\n", pro)

	// <.
	// 完全匹配
	// ???? todo
	var pro1 Product
	result := db.Where("code <> ?", "S").First(&pro1)
	fmt.Printf("Code= <>S || %v\n", pro1)
	fmt.Printf("err %v\n", result.Error )

	// in
	var pros []Product
	db.Where("price IN ?", []int{11, 12}).Find(&pros)
	PrintProdecut(pros)

	// Like
	db.Where("code LIKE ?", "%S%").Find(&pros)
	PrintProdecut(pros)

	// Time

	// between

}

func TestQueryWithWhere2(t *testing.T) {
	// AND
	var pros []Product
	fmt.Println("AND:")
	db.Where("code = ? AND price > ?", "S11", 0).Find(&pros)
	PrintProdecut(pros)

	// Time
	lastWeek := time.Now().Add(- time.Hour * 24 * 7)
	db.Where("created_at > ?", lastWeek).Find(&pros)
	PrintProdecut(pros)

	// between
	fmt.Println("=============")
	today := time.Now()
	db.Where("created_at BETWEEN ? AND ?", lastWeek, today).Find(&pros)
	PrintProdecut(pros)

}

func (p *Product) Print() {
	fmt.Printf("pro: %v\n", *p)
}

func TestQueryWithWhere3(t *testing.T) {
	var pros []Product
	// 不支持查first
	//db.Where([]int64{1, 2, 11, 12}).Find(&pros) // 
	db.Where([]int64{ 2, 11, 12}).Find(&pros)
	PrintProdecut(pros)
	
	var pro1 Product
	db.Where(&Product{
		Code:  "S14",
		Price: 14,
	}).Find(&pro1)
	pro1.Print()

	var pro2 Product
	db.Where(map[string]any{"code":"s14", "price":14}).Find(&pro2)
	pro2.Print()

}