package model

import (
	"gorm.io/gorm"
)

type User struct {
	Name string `json:"name"`
	Password	string `json:"password"`
	gorm.Model
}

type Author struct {
	Name string
	Email string
}

type Blog struct {
	ID int
	Author Author `gorm:"embedded"`
	Upvotes int32
}


