package api

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"net/http"
	"web3/pkg/e"
	"web3/pkg/util/ctl"
	"web3/pkg/util/jwt"
	"web3/types"
)

func HandleCommon() gin.HandlerFunc{
	return func(c *gin.Context) {
		c.JSON(http.StatusOK, ctl.NewResponse(e.SUCCESS, nil))
	}
}

type HandlerFunc func(c *gin.Context)

/* 

*/
func HandleLogin() gin.HandlerFunc {
	return func(c *gin.Context) {
		var userReq types.UserLoginReq
		if err := c.ShouldBind(&userReq); err != nil {
			fmt.Println(err.Error())
		}
		token, err := jwt.GenerateToken(userReq.Username)
		if err != nil {
			c.JSON(http.StatusBadRequest, gin.H{"error":""})
		}
		userTokenData := types.UserTokenData{
			User:  userReq,
			Token: token,
		}
		c.JSON(http.StatusOK, ctl.DefaultResponse(userTokenData))
	}
}