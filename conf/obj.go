package conf

type Config struct {
	AppConfig `mapstructure:"app"`
	MySqlConfig `mapstructure:"mysql"`

}

type MySqlConfig struct {
	Name     string `mapstructure:"name"`
	Password string `mapstructure:"password"`
	Host     string `mapstructure:"host"`
	Port     int `mapstructure:"port"`
	DbName   string `mapstructure:"db_name"`
}

type AppConfig struct {
	Host string `mapstructure:"host"`
	Port string `mapstructure:"port"`
}
