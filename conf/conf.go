package conf

import (
	"fmt"
	"github.com/fsnotify/fsnotify"
	"github.com/spf13/viper"
)

//var NewViper *viper.Viper
var Conf = &Config{}

func InitConfig() {
	NewViper := viper.New()
	NewViper.SetConfigName("conf")
	NewViper.SetConfigType("json")
	NewViper.AddConfigPath("./conf")
	err := NewViper.ReadInConfig()
	if err != nil {
		if _, ok := err.(viper.ConfigFileNotFoundError); ok{
			fmt.Println("err file not found ")
		}else{
			fmt.Println("viper other error")
		}
		// return
	}
	if err := viper.Unmarshal(Conf); err != nil {
		panic("")
	}


	fmt.Println("conf name:", NewViper.GetString("name"))

	NewViper.WatchConfig()
	go NewViper.OnConfigChange(func(in fsnotify.Event) {
		// 重新读配置文件
		NewViper.ReadInConfig()
		fmt.Println("reload conf")
	})

}