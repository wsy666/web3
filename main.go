package main

import router "web3/routes"

func main() {
	r := router.NewRouter()

	_ = r.Run(":8888")
}
